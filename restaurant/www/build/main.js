webpackJsonp([5],{

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuotesService; });
var QuotesService = /** @class */ (function () {
    function QuotesService() {
        this.favouriteQuotes = [];
    }
    QuotesService.prototype.addToFavouriteQuotes = function (quote) {
        this.favouriteQuotes.push(quote);
        console.log(this.favouriteQuotes);
    };
    QuotesService.prototype.removeFromFavouritesQuotes = function (quote) {
        var position = this.favouriteQuotes.findIndex(function (quoteEl) {
            return quoteEl.id == quote.id;
        });
        this.favouriteQuotes.splice(position, 1);
    };
    QuotesService.prototype.getFavouritesQuotes = function () {
        return this.favouriteQuotes.slice();
    };
    QuotesService.prototype.isFavourite = function (quote) {
        return this.favouriteQuotes.find(function (quoteEl) {
            // console.log(quote.id +"=="+quoteEl.id);
            return quoteEl.id == quote.id;
        });
    };
    return QuotesService;
}());

//# sourceMappingURL=quotes.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsService; });
var SettingsService = /** @class */ (function () {
    function SettingsService() {
        this.altBackGround = false;
    }
    SettingsService.prototype.setBackGround = function (state) {
        this.altBackGround = state;
    };
    SettingsService.prototype.returnBackGround = function () {
        return this.altBackGround;
    };
    return SettingsService;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__choosetable_choosetable__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_cartservice__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__additems_additems__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_category_add_category__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__add_dish_add_dish__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DashboardPage = /** @class */ (function () {
    function DashboardPage(navCtrl, navParams, CartService, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.CartService = CartService;
        this.global = global;
        this.availableTables = [];
        this.chooseTablePage = __WEBPACK_IMPORTED_MODULE_2__choosetable_choosetable__["a" /* ChoosetablePage */];
        this.AddCategoryPage = __WEBPACK_IMPORTED_MODULE_5__add_category_add_category__["a" /* AddCategoryPage */];
        this.loginpage = __WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */];
        this.addDishPage = __WEBPACK_IMPORTED_MODULE_6__add_dish_add_dish__["a" /* addDishPage */];
        this.displaycart = [];
    }
    DashboardPage.prototype.ionViewWillEnter = function () {
        this.refreshTables();
    };
    DashboardPage.prototype.ngOnInit = function () {
    };
    DashboardPage.prototype.refreshTables = function () {
        this.carts = this.CartService.getCartData();
        this.displaycart = [];
        for (var key in this.carts) {
            this.displaycart.push(key);
        }
    };
    DashboardPage.prototype.onTableCick = function (tableNO) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__additems_additems__["a" /* AdditemsPage */], { tableNo: tableNO });
    };
    DashboardPage.prototype.getFinalPrice = function (tableNO) {
        return this.CartService.getTotalFinalPrice(tableNO);
    };
    DashboardPage.prototype.checkOut = function (tableNO) {
        this.CartService.clearCart(tableNO);
        this.refreshTables();
    };
    DashboardPage.prototype.logout = function () {
        var _this = this;
        this.global.showConfirmation("Logout", "Are you sure you want to Logout from Application?", function () {
            _this.navCtrl.setRoot(_this.loginpage);
            // this.navCtrl.popToRoot();
        }, function () { }, "Confirm", "Cancel");
        // $rootScope.logout = function () {
        //     var confirmPopup = $ionicPopup.confirm({
        //         title: 'Logout',
        //         template: 'Are you sure you want to Logout from Application?'
        //     });
        //     confirmPopup.then(function (res) {
        //         if (res) {
        //             $ionicHistory.clearCache();
        //             $ionicHistory.clearHistory();
        //             sessionStorage.clear();
        //             $state.go("Login");
        //         }
        //     });
        // }
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/dashboard/dashboard.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-buttons left>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-buttons end>\n        <button ion-button (click)="logout()">\n          <ion-icon class="ionicons" name="log-out"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-buttons>\n    <ion-title text-center>dashboard</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="appBackground" padding>\n  <button ion-button block color="secondary" type="button" [navPush]="chooseTablePage">Take Order</button>\n  <button ion-button block color="secondary" type="button" [navPush]="AddCategoryPage">Add Category</button>\n  <button ion-button block color="secondary" type="button" [navPush]="addDishPage">Add Dish</button>\n\n  <ion-list>\n    <ion-row class="row " wrap responsive-sm>\n      <ion-col class="ItemClass" *ngFor="let item of displaycart">\n        <ion-card no-padding class="bookedtablecard">\n          <ion-item>\n            <!-- <ion-icon name="cafe" item-start large></ion-icon> -->\n            <h2 text-center>Table no ::{{ item}}</h2>\n            <h4 text-left>Total Amount :: {{getFinalPrice(item)}}</h4>\n          </ion-item>\n            <ion-row>\n              <ion-col col-6>\n                <button ion-button icon-start clear item-end (click)="onTableCick(item)">\n                  <ion-icon name="add"></ion-icon>\n                  Add\n                </button>\n              </ion-col>\n              <ion-col col-6>\n                <button ion-button icon-start clear item-end (click)="checkOut(item)">\n                  <ion-icon name="checkbox"></ion-icon>\n                  Check Out\n                </button>\n              </ion-col>\n            </ion-row>\n        </ion-card>\n      </ion-col>\n      </ion-row>\n\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/dashboard/dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__Services_cartservice__["a" /* CartService */],
            __WEBPACK_IMPORTED_MODULE_7__shared_globalService__["a" /* GlobalService */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdditemsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_cartservice__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__viewcart_viewcart__ = __webpack_require__(308);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AdditemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdditemsPage = /** @class */ (function () {
    function AdditemsPage(navCtrl, CartService, navParams, global) {
        this.navCtrl = navCtrl;
        this.CartService = CartService;
        this.navParams = navParams;
        this.global = global;
        this.isbuttonpressed = false;
        this.selectedIndex = 0;
        this.cartid = "";
        this.cart = {};
        this.dishes = this.global.globalData.dishes;
        for (var i = 0; i < this.dishes.length; i++) {
            this.dishes[i]["active"] = false;
        }
        // console.log(this.dishes);
        this.cartid = this.navParams.get('tableNo');
    }
    AdditemsPage.prototype.addtocart = function (product) {
        this.CartService.addToCart(product, this.cartid);
    };
    AdditemsPage.prototype.checkifpresent = function (product) {
        return this.CartService.checkifpresent(product, this.cartid);
    };
    AdditemsPage.prototype.removeFromcart = function (product) {
        this.CartService.removeFromcart(product, this.cartid);
    };
    AdditemsPage.prototype.getQuantity = function (product) {
        return this.CartService.getQuantity(product, this.cartid);
    };
    AdditemsPage.prototype.getCartQuantity = function () {
        return this.CartService.getCartQuantity(this.cartid);
    };
    AdditemsPage.prototype.selectCategoty = function (item, index) {
        this.selectedIndex = index;
    };
    AdditemsPage.prototype.getitemscount = function () {
        return this.CartService.getitemscount(this.cartid);
    };
    AdditemsPage.prototype.Viewcart = function () {
        var quantiy = this.getCartQuantity();
        if (quantiy > 0) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__viewcart_viewcart__["a" /* ViewcartPage */], { tableNo: this.cartid });
        }
        else {
            this.global.showAlert("Cart Empty", "Nothing in cart");
        }
    };
    AdditemsPage.prototype.toggleGroup = function (group) {
        for (var i = 0; i < this.dishes.length; i++) {
            //close all other active groups
            if (this.dishes[i].name != group.name)
                this.dishes[i].active = false;
        }
        group.active = !group.active;
        console.log(group);
    };
    AdditemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-additems',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/additems/additems.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-buttons end>\n      <button ion-button (click)="Viewcart()">\n        <ion-icon class="ionicons" name="cart"></ion-icon>\n        <ion-badge class="ionbadge" color="lighter" item-end>{{getitemscount()}}</ion-badge>\n      </button>\n    </ion-buttons>\n    <ion-title>Add Items</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="appBackground">\n  <ion-grid no-padding>\n    <ion-row>\n      <ion-col col-2>\n        <div class="fixed">\n          <ion-row class="row " color="lighter" *ngFor="let outerItem of dishes; let y = index">\n            <button ion-button block class="menubutton" (click)="selectCategoty(item,y)" [ngClass]="{\'active2\': selectedIndex === y}">\n              {{outerItem.name}}\n            </button>\n          </ion-row>\n        </div>\n      </ion-col>\n      <ion-col col-10>\n        <ion-row class="row " wrap responsive-sm>\n          <ion-col class="ItemClass" color="lighter" *ngFor="let item of dishes[selectedIndex].value; let i = index">\n            <ion-card>\n              <img [src]="item.imgUrl">\n              <ion-card-content text-center>\n                <p>{{item.name}}</p>\n              </ion-card-content>\n              <button ion-button block small *ngIf="!checkifpresent(item); else updateQuantity" (click)="addtocart(item)">Add\n                to Cart\n              </button>\n              <ng-template #updateQuantity>\n                <ion-grid no-padding class="buttongrid">\n                  <ion-row>\n                    <ion-col col-4>\n                      <button ion-button icon-only block small (click)="removeFromcart(item)">\n                        <ion-icon name="remove"></ion-icon>\n                      </button>\n                    </ion-col>\n                    <ion-col col-4 text-center>\n                      <div class="quantity">{{getQuantity(item)}}</div>\n                    </ion-col>\n                    <ion-col col-4>\n                      <button ion-button icon-only block small (click)="addtocart(item)">\n                        <ion-icon name="add"></ion-icon>\n                      </button>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ng-template>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/additems/additems.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* trigger */])('expand', [
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* state */])('true', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_21" /* style */])({ display: 'inline-flex' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* state */])('false', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_21" /* style */])({ display: 'none' })),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_22" /* transition */])('void => *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* animate */])('0s')),
                    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_22" /* transition */])('* <=> *', Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* animate */])('250ms ease-in-out'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__Services_cartservice__["a" /* CartService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globalService__["a" /* GlobalService */]])
    ], AdditemsPage);
    return AdditemsPage;
}());

//# sourceMappingURL=additems.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavouritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_quotes__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__quote_quote__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_settings__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FavouritesPage = /** @class */ (function () {
    function FavouritesPage(navCtrl, navParams, QuotesService, modalCtrl, settingsservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.QuotesService = QuotesService;
        this.modalCtrl = modalCtrl;
        this.settingsservice = settingsservice;
        this.quotes = [];
    }
    FavouritesPage.prototype.ionViewWillEnter = function () {
        this.quotes = this.QuotesService.getFavouritesQuotes();
    };
    FavouritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LibraryPage');
    };
    FavouritesPage.prototype.onQuoteClick = function (quote) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__quote_quote__["a" /* QuotePage */], quote);
        modal.present();
        modal.onDidDismiss(function (remove) {
            if (remove) {
                _this.onRemoveFromFavourites(quote);
            }
        });
    };
    FavouritesPage.prototype.onRemoveFromFavourites = function (quote) {
        this.QuotesService.removeFromFavouritesQuotes(quote);
        var position = this.quotes.findIndex(function (quoteEl) {
            return quoteEl.id == quote.id;
        });
        this.quotes.splice(position, 1);
    };
    FavouritesPage.prototype.getBackground = function () {
        return this.settingsservice.returnBackGround() ? 'altquotebackground' : 'quotebackground';
    };
    FavouritesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-favourites',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/favourites/favourites.html"*/'<!--\n  Generated template for the FavouritesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Favourites</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item-sliding *ngFor="let qoutes of quotes;let i =index" >\n          <ion-item  [color]="getBackground()" (click)="onQuoteClick(qoutes)">\n            <ion-icon item-start name="blank">\n              #{{i+1}}\n            </ion-icon>\n            <div padding>\n              <h2>{{qoutes.text}}</h2>\n              <p class="author">{{qoutes.person}}</p>\n            </div>\n          </ion-item>\n          <ion-item-options>\n            <button ion-button color="danger" (click)="onRemoveFromFavourites(qoutes)">\n              <ion-icon name="trash"></ion-icon>\n              Delete\n            </button>\n          </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>  \n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/favourites/favourites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_quotes__["a" /* QuotesService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__services_settings__["a" /* SettingsService */]])
    ], FavouritesPage);
    return FavouritesPage;
}());

//# sourceMappingURL=favourites.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the QuotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuotePage = /** @class */ (function () {
    function QuotePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    QuotePage.prototype.ngOnInit = function () {
        this.qoutes = this.navParams.data;
    };
    QuotePage.prototype.onClose = function (remove) {
        if (remove === void 0) { remove = false; }
        this.viewCtrl.dismiss(remove);
    };
    QuotePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-quote',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/quote/quote.html"*/'<!--\n  Generated template for the QuotePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Quote</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding text-center>\n    <ion-card >\n        <ion-card-header text-center>\n            <p>{{qoutes.person}}</p>\n        </ion-card-header>\n        <ion-card-content>\n          <h2>{{qoutes.text}}</h2>\n        </ion-card-content>\n        <ion-row>\n          <ion-col text-right>\n            <button ion-button clear (click)="onClose(true)">Unfavourite</button>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n      <button ion-button color="danger" (click)="onClose()">Close</button>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/quote/quote.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
    ], QuotePage);
    return QuotePage;
}());

//# sourceMappingURL=quote.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuotesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_quotes__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { GlobalService } from '../../shared/globalService';
/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QuotesPage = /** @class */ (function () {
    function QuotesPage(
    // private global:GlobalService,
    navCtrl, aletCtrl, navParams, QuotesService) {
        this.navCtrl = navCtrl;
        this.aletCtrl = aletCtrl;
        this.navParams = navParams;
        this.QuotesService = QuotesService;
    }
    QuotesPage.prototype.ngOnInit = function () {
        this.category = this.navParams.data;
    };
    QuotesPage.prototype.onFavouriteClick = function (quotes) {
        var _this = this;
        var confirm = this.aletCtrl.create({
            title: 'favourite Quote?',
            message: 'Do you really want to add this quote to your favourites list ?',
            buttons: [
                {
                    text: 'Disagree',
                    role: 'cancel',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        console.log('Agree clicked');
                        _this.QuotesService.addToFavouriteQuotes(quotes);
                    }
                }
            ]
        });
        confirm.present();
    };
    QuotesPage.prototype.onUnFavouriteClick = function (quote) {
        this.QuotesService.removeFromFavouritesQuotes(quote);
    };
    QuotesPage.prototype.isFavourite = function (quote) {
        return this.QuotesService.isFavourite(quote);
    };
    QuotesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-quotes',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/quotes/quotes.html"*/'<!--\n  Generated template for the QuotesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{category.category}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-card *ngFor="let qoutes of category.quotes;let i =index">\n        <ion-card-header text-center>\n          #{{i+1}}\n        </ion-card-header>\n        <ion-card-content>\n          <h2>{{qoutes.text}}</h2>\n          <p class="author">{{qoutes.person}}</p>\n        </ion-card-content>\n        <ion-row>\n          <ion-col text-right>\n            <button ion-button clear *ngIf="!isFavourite(qoutes)" (click)="onFavouriteClick(qoutes)">Favourite</button>\n            <button ion-button clear *ngIf="isFavourite(qoutes)" color="danger" (click)="onUnFavouriteClick(qoutes)">Unfavourite</button>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/quotes/quotes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_quotes__["a" /* QuotesService */]])
    ], QuotesPage);
    return QuotesPage;
}());

// this.global.showConfirmation("23","2132",this.onUnFavouriteClick(quotes),this.isFavourite(quotes),"damn","naam");
// this.global.showAlert("favourite Quote?","Do you really want to add this quote to your favourites list ?",
//       ()=>{
//           console.log('Agree clicked');
//           this.QuotesService.addToFavouriteQuotes(quotes);
//           this.global.showLoading();
//           var params = {
//             "Username": 'aa',
//             "Password": 'aa',
//             "simno": "123123213213"
//         };
//         var postData = {};
//         postData["JsonData"] = JSON.stringify(params);
//           this.global.httpPOST({
//             url:"MobileLogin",
//             data:postData,
//             success:(data)=>{
//               this.global.hideLoading();
//               console.log(data);
//             },
//             error:(error)=>{
//               console.log(error);
//             }
//           });
//         },"ok");
//# sourceMappingURL=quotes.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_settings__ = __webpack_require__(137);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl, navParams, SettingsService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.SettingsService = SettingsService;
    }
    SettingsPage.prototype.onChange = function (toggle) {
        this.SettingsService.setBackGround(toggle.checked);
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.isAltBackGroundChecked = function () {
        return this.SettingsService.returnBackGround();
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/settings/settings.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>settings</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-label>Alternative background</ion-label>\n      </ion-col>\n      <ion-col col-2>\n        <ion-toggle (ionChange)="onChange($event)" [checked]="isAltBackGroundChecked()"></ion-toggle>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__services_settings__["a" /* SettingsService */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__favourites_favourites__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__library_library__ = __webpack_require__(254);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.favouriitespage = __WEBPACK_IMPORTED_MODULE_2__favourites_favourites__["a" /* FavouritesPage */];
        this.librarypages = __WEBPACK_IMPORTED_MODULE_3__library_library__["a" /* LibraryPage */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabsPage');
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/tabs/tabs.html"*/'<ion-tabs >\n  <ion-tab [root]="librarypages" tabTitle="Library" tabIcon="bookmark"></ion-tab>\n  <ion-tab [root]="favouriitespage" tabTitle="Favourites" tabIcon="star"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/tabs/tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 211:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 211;

/***/ }),

/***/ 253:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/favourites/favourites.module": [
		536,
		4
	],
	"../pages/quote/quote.module": [
		537,
		3
	],
	"../pages/quotes/quotes.module": [
		538,
		2
	],
	"../pages/settings/settings.module": [
		539,
		1
	],
	"../pages/tabs/tabs.module": [
		540,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 253;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 254:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LibraryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_qoutes__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__quotes_quotes__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LibraryPage = /** @class */ (function () {
    function LibraryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.quotespage = __WEBPACK_IMPORTED_MODULE_3__quotes_quotes__["a" /* QuotesPage */];
    }
    LibraryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LibraryPage');
    };
    LibraryPage.prototype.ionViewWillEnter = function () {
        this.quotescollection = __WEBPACK_IMPORTED_MODULE_2__data_qoutes__["a" /* default */];
    };
    LibraryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-library',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/library/library.html"*/'<!--\n  Generated template for the LibraryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons start>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n    <ion-title>library</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h3 text-align-center>Select Your Favurites Quotes</h3>\n  <ion-list>\n    <button ion-item *ngFor="let category of quotescollection" [navPush]="quotespage" [navParams]="category">\n        <ion-icon name="{{category.icon}}" item-start></ion-icon>\n          <h2>{{category.category |uppercase}}</h2>\n          <p>{{category.quotes.length}}</p>\n        <ion-icon ios="ios-arrow-round-forward" md="md-arrow-round-forward" item-end></ion-icon>\n    </button>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/pages/library/library.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], LibraryPage);
    return LibraryPage;
}());

//# sourceMappingURL=library.js.map

/***/ }),

/***/ 28:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_crypto_js__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_crypto_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_crypto_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(306);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { Http, Headers, RequestOptions } from '@angular/http';



// import { category } from '../interfaces/category';
var GlobalService = /** @class */ (function () {
    function GlobalService(platform, alertCtrl, loadingCtrl, httpClient) {
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.httpClient = httpClient;
        this.dynamicList = {};
        this.constants = {
            "appName": "Restaurant Counter App",
            "appVersion": "0.0.1",
            "responseType": "json",
            "responseTimeOut": 120000,
            "errorAlertHeader": "Error",
            "infoAlertHeader": "Info",
            // URLS Links
            // "appURL":"http://192.168.2.11:8080/registration/webapi/",
            // "appURL": "http://support.warithsoftware.com/service.asmx/",
            "appURL": "http://msd-testing.000webhostapp.com/",
            "urls": {
                "adddish": "api/restaurant/addDish.php",
                "imageUpload": "https://msd-testing.000webhostapp.com/api/restaurant/imageUpload.php",
                "getCategory": "api/restaurant/getCategory.php",
                "addCategory": "api/restaurant/addCategory.php",
                "getDishes": "api/restaurant/getDishes.php",
                "signup": "api/restaurant/signup.php",
                "prelogin": "api/restaurant/prelogin.php",
                "login": "api/restaurant/login.php",
                "checkout": "api/restaurant/checkout.php"
            },
            "Environment": "PROD",
            // LocalStorage Keys
            "ls_userdetais": "userDetails",
            "ls_uid": "uid",
            "ls_restaurantname": "restaurant_name",
            "ls_nooftables": "nooftables",
        };
        this.globalData = {
            tables: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            bookedTables: [],
            dishes: [{
                    name: "Rice",
                    value: [
                        {
                            id: "1",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 10,
                        },
                        {
                            id: "2",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 30,
                        },
                        {
                            id: "3",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "4",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "5",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "6",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "7",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "8",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "9",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "10",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "11",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "12",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "13",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "14",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        }
                    ],
                }, {
                    name: "Gravy",
                    value: [
                        {
                            id: "15",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "16",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "17",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "18",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "19",
                            imgUrl: "assets/imgs/biryani.jpg",
                            name: "Biryani",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "20",
                            imgUrl: "assets/imgs/mandi.jpg",
                            name: "Mandi",
                            rate: 60,
                            discount: 0,
                        },
                        {
                            id: "21",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        }
                    ],
                }, {
                    name: "starter",
                    value: [
                        {
                            id: "22",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        }
                    ],
                }, {
                    name: "drinks",
                    value: [
                        {
                            id: "23",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        }
                    ],
                }, {
                    name: "juices",
                    value: [
                        {
                            id: "24",
                            imgUrl: "assets/imgs/tahari.jpg",
                            name: "Tahari",
                            rate: 60,
                            discount: 0,
                        }
                    ],
                }
            ]
        };
    }
    GlobalService.prototype.httpPOST = function (options) {
        // var headers = new Headers();
        // var postData = {};
        // headers.append("Accept", 'application/json');
        // headers.append('Content-Type', 'application/json' );
        // const requestOptions = new RequestOptions({ headers: headers });
        this.httpClient.post(this.constants.appURL + options.url, options.data)
            .subscribe(function (data) {
            options.success(data);
            // console.log(data);
        }, function (error) {
            options.error(error);
            // console.log(error);
        });
    };
    GlobalService.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            spinner: "bubbles",
            duration: 30000,
            cssClass: '',
            showBackdrop: true,
            enableBackdropDismiss: false,
        });
        this.loading.present();
        this.loading.onDidDismiss(function () {
            // console.log('Dismissed loading');
        });
    };
    GlobalService.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    GlobalService.prototype.setInLocalStorage = function (key, value) {
        if (this.platform.is('ios')) {
            window.localStorage.setItem(key, value);
        }
        else {
            window.localStorage.setItem(key, this.encryptString(value));
        }
    };
    GlobalService.prototype.getFromLocalStorage = function (key) {
        if (this.platform.is('ios')) {
            if (window.localStorage.getItem(key) == null) {
                return "";
            }
            else {
                return window.localStorage.getItem(key);
            }
        }
        else {
            if (window.localStorage.getItem(key) == null) {
                return "";
            }
            else {
                return this.decryptString(window.localStorage.getItem(key));
            }
        }
    };
    GlobalService.prototype.setInLocalStorageObject = function (key, value) {
        if (this.platform.is('ios')) {
            if (value && Object.keys(value).length > 0) {
                window.localStorage.setItem(key, JSON.stringify(value));
            }
        }
        else {
            var encryptedJson = this.encryptJson(value);
            if (encryptedJson && encryptedJson.length > 0) {
                window.localStorage.setItem(key, encryptedJson);
            }
        }
    };
    GlobalService.prototype.getFromLocalStorageObject = function (key) {
        if (this.platform.is('ios')) {
            if (window.localStorage.getItem(key) == null) {
                return {};
            }
            else {
                return window.localStorage.getItem(key) ? JSON.parse(window.localStorage.getItem(key)) : {};
            }
        }
        else {
            if (window.localStorage.getItem(key) == null) {
                return {};
            }
            else {
                return window.localStorage.getItem(key) ? JSON.parse(this.decryptString(window.localStorage.getItem(key))) : {};
            }
        }
    };
    GlobalService.prototype.encryptJson = function (textToEncrypt) {
        var encrypted = "";
        var key = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["enc"].Hex.parse("50524f442354435340616c6232303133");
        encrypted = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["AES"].encrypt(JSON.stringify(textToEncrypt), key, {
            mode: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["mode"].CBC,
            padding: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["pad"].Pkcs7,
            iv: key,
        });
        return encrypted.toString();
    };
    GlobalService.prototype.encryptString = function (textToEncrypt) {
        var encrypted = "";
        var key = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["enc"].Hex.parse("50524f442354435340616c6232303133");
        encrypted = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["AES"].encrypt(textToEncrypt, key, {
            mode: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["mode"].CBC,
            padding: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["pad"].Pkcs7,
            iv: key,
        });
        return encrypted.toString();
    };
    GlobalService.prototype.decryptString = function (textToDecrypt) {
        var key = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["enc"].Hex.parse("50524f442354435340616c6232303133");
        var decryptedText = __WEBPACK_IMPORTED_MODULE_2_crypto_js__["AES"].decrypt(textToDecrypt, key, {
            mode: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["mode"].CBC,
            padding: __WEBPACK_IMPORTED_MODULE_2_crypto_js__["pad"].Pkcs7,
            iv: key,
        });
        return decryptedText.toString(__WEBPACK_IMPORTED_MODULE_2_crypto_js__["enc"].Utf8);
    };
    GlobalService.prototype.getDynamicListData = function () {
        var copyOfDynamicList = Object.assign({}, this.dynamicList);
        return copyOfDynamicList;
    };
    GlobalService.prototype.setDynamicListData = function (dynamicList) {
        this.dynamicList = dynamicList;
    };
    GlobalService.prototype.generateUniqueId = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 17; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    GlobalService.prototype.showAlert = function (title, message, callBack, okText) {
        var confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: okText || 'Ok',
                    handler: function () {
                        callBack && callBack();
                    }
                }
            ]
        });
        confirm.present();
    };
    GlobalService.prototype.showConfirmation = function (title, message, confirmCallBack, CancelCallBack, okText, cancelText) {
        var confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: cancelText || 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        CancelCallBack && CancelCallBack();
                    }
                },
                {
                    text: okText || 'Ok',
                    handler: function () {
                        confirmCallBack && confirmCallBack();
                    }
                }
            ]
        });
        confirm.present();
    };
    GlobalService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], GlobalService);
    return GlobalService;
}());

//# sourceMappingURL=globalService.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shopping_list_shopping_list__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__receipes_receipes__ = __webpack_require__(301);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabPage = /** @class */ (function () {
    function TabPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shoppingListpage = __WEBPACK_IMPORTED_MODULE_2__shopping_list_shopping_list__["a" /* ShoppingListPage */];
        this.receipesPage = __WEBPACK_IMPORTED_MODULE_3__receipes_receipes__["a" /* ReceipesPage */];
    }
    TabPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabPage');
    };
    TabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tab',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/tab/tab.html"*/'<ion-tabs>\n  <ion-tab [root]="shoppingListpage" tabTitle="Shopping List" tabIcon="list-box"></ion-tab>\n  <ion-tab [root]="receipesPage" tabTitle="Receipes" tabIcon="pizza"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/tab/tab.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TabPage);
    return TabPage;
}());

//# sourceMappingURL=tab.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShoppingListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { ShoppingListService } from '../../service/shoppingList.Service';
var ShoppingListPage = /** @class */ (function () {
    function ShoppingListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ShoppingListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShoppingListPage');
    };
    ShoppingListPage.prototype.onAddItem = function (form) {
        console.log(form.value);
    };
    ShoppingListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shopping-list',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/shopping-list/shopping-list.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title text-center>shopping-list</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n  <form #form="ngForm" (ngSubmit)="onAddItem(form)">\n    <ion-list>\n      <ion-item>\n        <ion-label floating>Ingredient Name</ion-label>\n        <ion-input type="text" name="ingredientName" ngModel required></ion-input>\n      </ion-item>\n      \n      <ion-item>\n          <ion-label floating>Amount</ion-label>\n          <ion-input type="number" name="amount" ngModel required></ion-input>\n      </ion-item>\n    </ion-list>\n    <button ion-button type="submit" block [disabled]="!form.valid"> Add Item</button>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/shopping-list/shopping-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ShoppingListPage);
    return ShoppingListPage;
}());

//# sourceMappingURL=shopping-list.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceipesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ReceipesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReceipesPage = /** @class */ (function () {
    function ReceipesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReceipesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReceipesPage');
    };
    ReceipesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-receipes',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/receipes/receipes.html"*/'<!--\n  Generated template for the ReceipesPage page.\n\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <ion-buttons start>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>receipes</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/receipes/receipes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ReceipesPage);
    return ReceipesPage;
}());

//# sourceMappingURL=receipes.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prelogin_prelogin__ = __webpack_require__(311);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signup = __WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */];
        this.login = __WEBPACK_IMPORTED_MODULE_3__prelogin_prelogin__["a" /* PreloginPage */];
    }
    RegistrationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistrationPage');
    };
    RegistrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registration',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/registration/registration.html"*/'<ion-slides pager class="appBackground">\n\n    <ion-slide >\n      <img id="logo" class="iconImage" src="assets/imgs/icon.png">\n      <button ion-button color="secondary" type="button" [navPush]="signup">Sign Up</button>\n    </ion-slide>\n  \n    <ion-slide >\n        <img id="logo" class="iconImage" src="assets/imgs/icon.png">\n        <button ion-button color="secondary" type="button" [navPush]="login">Login</button>\n    </ion-slide>\n      \n  \n  </ion-slides>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/registration/registration.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], RegistrationPage);
    return RegistrationPage;
}());

//# sourceMappingURL=registration.js.map

/***/ }),

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.imageUrl = "assets/imgs/click-me.png";
        this.dashboard = __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */];
        this.tableList = this.global.globalData.tables;
    }
    SignupPage.prototype.selectImage = function () {
    };
    SignupPage.prototype.onSubmit = function (formData) {
        var _this = this;
        this.global.showLoading();
        var params = {
            "name": formData.value.name,
            "nooftable": formData.value.nooftable,
            "email": formData.value.email,
            "pin": formData.value.pin,
        };
        this.global.httpPOST({
            url: this.global.constants.urls.signup,
            data: JSON.stringify(params),
            success: function (data) {
                _this.global.hideLoading();
                if (data.SUCCESS == "1") {
                    _this.global.showAlert(_this.global.constants.infoAlertHeader, data.message);
                    _this.global.setInLocalStorageObject(_this.global.constants.ls_userdetais, {
                        uid: data.uid,
                        name: formData.value.name,
                        nooftables: formData.value.nooftable
                    });
                    // this.global.setInLocalStorage(this.global.constants.ls_uid,data.uid);
                    // this.global.setInLocalStorage(this.global.constants.ls_restaurantname,formData.value.name);
                    // this.global.setInLocalStorage(this.global.constants.ls_nooftables,formData.value.nooftable);
                    _this.navCtrl.setRoot(_this.dashboard);
                }
                else {
                    _this.global.showAlert(_this.global.constants.errorAlertHeader, data.message);
                }
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/signup/signup.html"*/'\n<ion-header>\n    <ion-navbar color="primary">\n      <ion-title>Sign Up</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="appBackground">\n    <img [src]="imageUrl" (click)="selectImage()" class="categoryImage" alt="your image">\n    <form #form="ngForm"  (ngSubmit)="onSubmit(form)">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Restaurant Name</ion-label>\n                <ion-input type="text" name="name" ngModel required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Number Of Tables</ion-label>\n                <ion-select  name="nooftable" ngModel required>\n                    <ion-option *ngFor="let item of tableList" [value]="item">{{item}}</ion-option>\n                </ion-select>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Email Id</ion-label>\n                <ion-input type="email" name="email" ngModel required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Pin</ion-label>\n                <ion-input type="tel" name="pin" ngModel required></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button  color="secondary" type="submit" block [disabled]="!form.valid">Sign Up</button>       \n    </form>\n</ion-content>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globalService__["a" /* GlobalService */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChoosetablePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__additems_additems__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_cartservice__ = __webpack_require__(74);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChoosetablePage = /** @class */ (function () {
    function ChoosetablePage(navCtrl, navParams, global, CartService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.CartService = CartService;
        this.availableTables = [];
    }
    ChoosetablePage.prototype.ionViewWillEnter = function () {
        var bookedTables = this.CartService.getBookedTables();
        this.availableTables = this.global.globalData.tables.filter(function (val) { return !bookedTables.includes(val); });
    };
    ChoosetablePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChoosetablePage');
    };
    ChoosetablePage.prototype.onTableCick = function (tableNO) {
        var _this = this;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__additems_additems__["a" /* AdditemsPage */], { tableNo: tableNO });
        this.global.showLoading();
        this.global.httpPOST({
            url: this.global.constants.urls.getDishes,
            success: function (data) {
                _this.global.hideLoading();
                console.log(data);
                //   console.log(this.formatResult(data.records));
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    ChoosetablePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-choosetable',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/choosetable/choosetable.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Choose Table</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="appBackground" padding>\n  <ion-list >\n    <ion-item col-6  class="tableClass" color="lighter" *ngFor="let item of availableTables; let i = index" (click)="onTableCick(item)">\n      <ion-icon item-start name="football">\n      </ion-icon>\n      <h2 text-center>{{item}}</h2>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/choosetable/choosetable.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globalService__["a" /* GlobalService */],
            __WEBPACK_IMPORTED_MODULE_4__Services_cartservice__["a" /* CartService */]])
    ], ChoosetablePage);
    return ChoosetablePage;
}());

//   var params = {
//     "Username": 'aa',
//     "Password": 'aa',
//     "simno": "123123213213"
// };
// var postData = {};
// postData["JsonData"] = JSON.stringify(params);
//   formatResult(resultArray){
//     var result=[]
//     resultArray.forEach(x => {
//         if(result.length>0){
//             let isPresent=0;
//             for(let i=0;i<result.length;i++){
//                 if(result[i].name==x.categoryName){
//                     result[i].value.push(x);
//                     isPresent++;
//                 }
//             }
//             if(isPresent==0){
//                 let xvalue=[];
//                 xvalue.push(x);
//                 result.push({
//                     name:x.categoryName,
//                     value:xvalue
//                 });
//             }
//         }else{
//             let xvalue=[];
//             xvalue.push(x);
//             result.push({
//                 name:x.categoryName,
//                 value:xvalue
//             });
//         }
//     });
//     return result;
// }
//# sourceMappingURL=choosetable.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewcartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_cartservice__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_globalService__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ViewcartPage = /** @class */ (function () {
    function ViewcartPage(navCtrl, navParams, CartService, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.CartService = CartService;
        this.global = global;
        this.cartid = "";
        this.cartid = this.navParams.get('tableNo');
        this.cart = this.CartService.getCart(this.cartid);
        this.cart.items = Object.values(this.cart.items);
        this.user = this.global.getFromLocalStorageObject(this.global.constants.ls_userdetais);
    }
    ViewcartPage.prototype.ionViewCanEnter = function () {
        var previous = this.navCtrl.last().name;
        if ("AdditemsPage" == previous) {
            return true;
        }
        else {
            return false;
        }
    };
    ViewcartPage.prototype.addtocart = function (product) {
        this.CartService.addToCart(product, this.cartid);
    };
    ViewcartPage.prototype.getQuantity = function (product) {
        return this.CartService.getQuantity(product, this.cartid);
    };
    ViewcartPage.prototype.removeFromcart = function (product) {
        this.CartService.removeFromcart(product, this.cartid);
    };
    ViewcartPage.prototype.getItemPrice = function (product) {
        return this.CartService.getItemPrice(product, this.cartid);
    };
    ViewcartPage.prototype.getTotalPrice = function () {
        return this.CartService.getTotalPrice(this.cartid);
    };
    ViewcartPage.prototype.getTotalDiscount = function () {
        return this.CartService.getTotalDiscount(this.cartid);
    };
    ViewcartPage.prototype.getTotalFinalPrice = function () {
        return this.CartService.getTotalFinalPrice(this.cartid);
    };
    ViewcartPage.prototype.getItemDiscount = function (product) {
        return this.CartService.getItemDiscount(product, this.cartid);
    };
    ViewcartPage.prototype.submit = function () {
        var _this = this;
        var params = {
            "customername": "Demo",
            "tableno": this.cartid,
            "total": this.getTotalPrice(),
            "discount": this.getTotalDiscount(),
            "finalamount": this.getTotalFinalPrice(),
            "totalquantity": this.cart.items.length,
            "uid": this.user.uid,
            "orderitems": this.cart.items,
        };
        this.global.httpPOST({
            url: this.global.constants.urls.checkout,
            data: JSON.stringify(params),
            success: function (data) {
                _this.CartService.clearCart(_this.cartid);
                _this.navCtrl.popToRoot();
                _this.global.showAlert(data.message, data.message);
                //   console.log(this.formatResult(data.records));
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    ViewcartPage.prototype.dashboard = function () {
        this.navCtrl.popToRoot();
    };
    ViewcartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewcart',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/viewcart/viewcart.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>View Cart</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  class="appBackground">\n  <ion-grid no-padding>\n    <ion-card>\n    <ion-row class="bgrow">\n      <ion-col text-center>\n        <p class="headingp">Bill for table no {{cartid}}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class="bgrow ">\n      <ion-col col-3 text-center>\n        <p class="columnp" >Name</p>\n      </ion-col>\n      <ion-col col-4 text-center>\n        <p class="columnp">Rate(Discount)</p>\n      </ion-col>\n      <ion-col col-2 text-center>\n        <p class="columnp">Qnty</p>\n      </ion-col>\n      <!-- <ion-col col-2 text-center>\n        <p>Discount</p>\n      </ion-col> -->\n      <ion-col col-3 text-center>\n        <p class="columnp">Price</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class="itemrow" *ngFor="let item of cart.items; let y = index">\n      <ion-col col-3 text-center>\n        <p>{{y+1 }}) {{ item.name}}</p>\n      </ion-col>\n      <ion-col col-4 text-center>\n        <p>{{item.rate}}({{item.discount}})</p>\n      </ion-col>\n      <ion-col col-2 text-center>\n        <p>{{item.quantity}}</p>\n      </ion-col>\n      <!-- <ion-col col-2 text-center>\n        <p>{{item.discount}}</p>\n      </ion-col> -->\n      <ion-col col-3 text-center>\n        <p>{{item.finalprice}}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class="margintop itemrow">\n      <ion-col col-6 text-left>\n        <p class="leftcolumn">Price</p>\n      </ion-col>\n      <ion-col col-6 text-right>\n        <p class="rightcolumn">₹ {{getTotalPrice()}}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class="itemrow">\n      <ion-col col-6 text-left>\n        <p class="leftcolumn">Discount</p>\n      </ion-col>\n      <ion-col col-6 text-right>\n        <p class="rightcolumn">₹ {{getTotalDiscount()}}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row class="bottomrow">\n      <ion-col col-6 text-left>\n        <p class="leftcolumn">Final Amount</p>\n      </ion-col>\n      <ion-col col-6 text-right>\n        <p class="rightcolumn">₹ {{getTotalFinalPrice()}}</p>\n      </ion-col>\n    </ion-row>\n    </ion-card>\n  </ion-grid>\n  <ion-row padding>\n      <ion-col>\n            <button ion-button block color="secondary" (click)="submit()" type="button" >Check Out</button>\n      </ion-col>\n      <ion-col>\n            <button ion-button block color="secondary" (click)="dashboard()" type="button" >Dashboard</button>\n      </ion-col>\n    </ion-row>\n  \n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/viewcart/viewcart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__Services_cartservice__["a" /* CartService */],
            __WEBPACK_IMPORTED_MODULE_3__shared_globalService__["a" /* GlobalService */]])
    ], ViewcartPage);
    return ViewcartPage;
}());

//# sourceMappingURL=viewcart.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_globalService__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AddCategoryPage = /** @class */ (function () {
    function AddCategoryPage(navCtrl, navParams, camera, androidPermissions, transfer, DomSanitizer, base64, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.androidPermissions = androidPermissions;
        this.transfer = transfer;
        this.DomSanitizer = DomSanitizer;
        this.base64 = base64;
        this.global = global;
        this.imageUrl = "assets/imgs/click-me.png";
        this.options = {
            quality: 10,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            allowEdit: true
        };
    }
    AddCategoryPage.prototype.ionViewWillEnter = function () {
        this.userDetails = this.global.getFromLocalStorageObject(this.global.constants.ls_userdetais);
    };
    AddCategoryPage.prototype.selectImage = function () {
        var _this = this;
        this.global.showLoading();
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(function (result) { return console.log('Has permission?', result.hasPermission); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.CAMERA); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
        this.camera.getPicture(this.options).then(function (imageData) {
            _this.imageData = imageData;
            _this.base64.encodeFile(imageData).then(function (base64File) {
                _this.imageUrl = base64File;
                _this.global.hideLoading();
            }, function (err) {
                _this.global.hideLoading();
                // console.log(err);
            });
        }, function (err) {
            _this.global.hideLoading();
            // Handle error
        });
    };
    AddCategoryPage.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form);
        this.global.showLoading();
        var fileTransfer = this.transfer.create();
        var options1 = {
            fileKey: 'file',
            fileName: 'name.jpg',
            headers: {}
        };
        fileTransfer.upload(this.imageData, this.global.constants.urls.imageUpload, options1)
            .then(function (imageData) {
            var res = JSON.parse(imageData.response);
            if (res.status == 'SUCCESS') {
                var params = {
                    "name": form.value.category,
                    "image": res.imageName,
                    "uid": _this.userDetails.uid
                };
                // var postData = {};
                // postData["JsonData"] = JSON.stringify(params);
                _this.global.httpPOST({
                    url: _this.global.constants.urls.addCategory,
                    data: JSON.stringify(params),
                    success: function (data) {
                        _this.global.hideLoading();
                        _this.global.showAlert(data.message, data.message);
                        console.log(data);
                        //   console.log(this.formatResult(data.records));
                    },
                    error: function (error) {
                        _this.global.hideLoading();
                        console.log(error);
                    }
                });
            }
            else {
                _this.global.hideLoading();
                _this.global.showAlert(res.status, res.message);
            }
        }, function (err) {
            _this.global.hideLoading();
            // error
            // console.log(err);
        });
    };
    AddCategoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-category',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/add-category/add-category.html"*/'<ion-header>\n    <ion-navbar color="primary">\n      <ion-title>Add Category</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="appBackground">\n    <img [src]="DomSanitizer.bypassSecurityTrustUrl(imageUrl)" (click)="selectImage()" class="categoryImage" alt="your image">\n    <form #form="ngForm"  (ngSubmit)="onSubmit(form)">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Category</ion-label>\n                <ion-input type="text" name="category" ngModel required></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button  color="secondary" type="submit" block [disabled]="!form.valid"> Add Category</button>       \n    </form>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/add-category/add-category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__["a" /* Base64 */],
            __WEBPACK_IMPORTED_MODULE_7__shared_globalService__["a" /* GlobalService */]])
    ], AddCategoryPage);
    return AddCategoryPage;
}());

//# sourceMappingURL=add-category.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return addDishPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_globalService__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var addDishPage = /** @class */ (function () {
    function addDishPage(navCtrl, navParams, camera, androidPermissions, transfer, DomSanitizer, base64, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.androidPermissions = androidPermissions;
        this.transfer = transfer;
        this.DomSanitizer = DomSanitizer;
        this.base64 = base64;
        this.global = global;
        this.imageUrl = "assets/imgs/click-me.png";
        this.options = {
            quality: 10,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            allowEdit: true
        };
    }
    addDishPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.global.showLoading();
        this.global.httpPOST({
            url: this.global.constants.urls.getCategory,
            success: function (data) {
                _this.global.hideLoading();
                _this.categorylist = data.categorylist;
                console.log(data);
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    addDishPage.prototype.selectImage = function () {
        var _this = this;
        this.global.showLoading();
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(function (result) { return console.log('Has permission?', result.hasPermission); }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.CAMERA); });
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
        this.camera.getPicture(this.options).then(function (imageData) {
            _this.imageData = imageData;
            _this.base64.encodeFile(imageData).then(function (base64File) {
                _this.imageUrl = base64File;
                _this.global.hideLoading();
            }, function (err) {
                _this.global.hideLoading();
                // console.log(err);
            });
        }, function (err) {
            _this.global.hideLoading();
            // Handle error
        });
    };
    addDishPage.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form);
        this.global.showLoading();
        var fileTransfer = this.transfer.create();
        var options1 = {
            fileKey: 'file',
            fileName: 'name.jpg',
            headers: {}
        };
        fileTransfer.upload(this.imageData, this.global.constants.urls.imageUpload, options1)
            .then(function (imageData) {
            var res = JSON.parse(imageData.response);
            if (res.status == 'SUCCESS') {
                var params = {
                    "name": form.value.name,
                    "rate": form.value.rate,
                    "discount": form.value.discount,
                    "category": form.value.category,
                    "image": res.imageName
                };
                _this.global.httpPOST({
                    url: _this.global.constants.urls.adddish,
                    data: JSON.stringify(params),
                    success: function (data) {
                        _this.global.hideLoading();
                        _this.navCtrl.popToRoot();
                        _this.global.showAlert(data.message, data.message);
                        //   console.log(this.formatResult(data.records));
                    },
                    error: function (error) {
                        _this.global.hideLoading();
                        console.log(error);
                    }
                });
            }
            else {
                _this.global.hideLoading();
                _this.global.showAlert(res.status, res.message);
            }
        }, function (err) {
            _this.global.hideLoading();
            // error
            // console.log(err);
        });
    };
    addDishPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-dish',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/add-dish/add-dish.html"*/'<ion-header>\n    <ion-navbar color="primary">\n      <ion-title>Add Dish</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="appBackground">\n    <img [src]="DomSanitizer.bypassSecurityTrustUrl(imageUrl)" (click)="selectImage()" class="categoryImage" alt="your image">\n    <form #form="ngForm"  (ngSubmit)="onSubmit(form)">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Name</ion-label>\n                <ion-input type="text" name="name" ngModel required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Rate</ion-label>\n                <ion-input type="tel" name="rate" ngModel required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Discount</ion-label>\n                <ion-input type="tel" name="discount" ngModel required></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label floating>Category</ion-label>\n                <ion-select  name="category" ngModel required>\n                    <ion-option *ngFor="let item of categorylist" [value]="item.id">{{item.name}}</ion-option>\n                </ion-select>\n            </ion-item>\n        </ion-list>\n        <button ion-button  color="secondary" type="submit" block [disabled]="!form.valid"> Add Dish</button>       \n    </form>\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/add-dish/add-dish.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__["a" /* Base64 */],
            __WEBPACK_IMPORTED_MODULE_7__shared_globalService__["a" /* GlobalService */]])
    ], addDishPage);
    return addDishPage;
}());

//# sourceMappingURL=add-dish.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreloginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PreloginPage = /** @class */ (function () {
    function PreloginPage(navCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.imageUrl = "assets/imgs/click-me.png";
        this.login = __WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */];
    }
    PreloginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    PreloginPage.prototype.onSubmit = function (formData) {
        var _this = this;
        this.global.showLoading();
        var params = {
            "email": formData.value.email,
        };
        this.global.httpPOST({
            url: this.global.constants.urls.prelogin,
            data: JSON.stringify(params),
            success: function (data) {
                _this.global.hideLoading();
                console.log(data);
                if (data.SUCCESS == "1") {
                    _this.global.setInLocalStorageObject(_this.global.constants.ls_userdetais, {
                        uid: data.uid,
                        name: data.name,
                        nooftables: data.nooftable
                    });
                    _this.navCtrl.push(_this.login);
                }
                else {
                    _this.global.showAlert(_this.global.constants.errorAlertHeader, data.message);
                }
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    PreloginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-prelogin',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/prelogin/prelogin.html"*/'\n<ion-header>\n    <ion-navbar color="primary">\n      <ion-title>Login</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="appBackground">\n    <img [src]="imageUrl" (click)="selectImage()" class="categoryImage" alt="your image">\n    <form #form="ngForm"  (ngSubmit)="onSubmit(form)">\n        <ion-list>\n            <ion-item>\n                <ion-label floating>Email Id</ion-label>\n                <ion-input type="email" name="email" ngModel required></ion-input>\n            </ion-item>\n            <!-- <ion-item>\n                <ion-label floating>Pin</ion-label>\n                <ion-input type="tel" name="pin" ngModel required></ion-input>\n            </ion-item> -->\n        </ion-list>\n        <button ion-button  color="secondary" type="submit" block [disabled]="!form.valid">Check Email\n        </button>       \n    </form>\n</ion-content>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/prelogin/prelogin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globalService__["a" /* GlobalService */]])
    ], PreloginPage);
    return PreloginPage;
}());

//# sourceMappingURL=prelogin.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(447);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_permissions__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_fcm__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_local_notifications__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(498);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_platform_browser_animations__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_library_library__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_quotes_quotes__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_quote_quote__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_favourites_favourites__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_tabs_tabs__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_quotes__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_settings__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__RecipeApp_pages_tab_tab__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__RecipeApp_pages_receipe_receipe__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__RecipeApp_pages_receipes_receipes__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__RecipeApp_pages_edit_receipe_edit_receipe__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__RecipeApp_pages_shopping_list_shopping_list__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__RecipeApp_service_shoppingList_Service__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__PDarbar_directives_on_valid_submit_on_valid_submit__ = __webpack_require__(535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__PDarbar_pages_dashboard_dashboard__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__PDarbar_pages_choosetable_choosetable__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__PDarbar_pages_additems_additems__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__PDarbar_Services_cartservice__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__PDarbar_pages_viewcart_viewcart__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__PDarbar_pages_add_category_add_category__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__PDarbar_pages_add_dish_add_dish__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__PDarbar_pages_registration_registration__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__PDarbar_pages_signup_signup__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__PDarbar_pages_login_login__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__PDarbar_pages_prelogin_prelogin__ = __webpack_require__(311);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













 //Dont delete this
 //Dont delete this
 //Dont delete this
// app related imports








// RecipeApp






// darbar












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_16__pages_library_library__["a" /* LibraryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_quotes_quotes__["a" /* QuotesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_quote_quote__["a" /* QuotePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_favourites_favourites__["a" /* FavouritesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_24__RecipeApp_pages_tab_tab__["a" /* TabPage */],
                __WEBPACK_IMPORTED_MODULE_25__RecipeApp_pages_receipe_receipe__["a" /* ReceipePage */],
                __WEBPACK_IMPORTED_MODULE_26__RecipeApp_pages_receipes_receipes__["a" /* ReceipesPage */],
                __WEBPACK_IMPORTED_MODULE_27__RecipeApp_pages_edit_receipe_edit_receipe__["a" /* EditReceipePage */],
                __WEBPACK_IMPORTED_MODULE_28__RecipeApp_pages_shopping_list_shopping_list__["a" /* ShoppingListPage */],
                // darbar
                __WEBPACK_IMPORTED_MODULE_31__PDarbar_pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_32__PDarbar_pages_choosetable_choosetable__["a" /* ChoosetablePage */],
                __WEBPACK_IMPORTED_MODULE_33__PDarbar_pages_additems_additems__["a" /* AdditemsPage */],
                __WEBPACK_IMPORTED_MODULE_35__PDarbar_pages_viewcart_viewcart__["a" /* ViewcartPage */],
                __WEBPACK_IMPORTED_MODULE_36__PDarbar_pages_add_category_add_category__["a" /* AddCategoryPage */],
                __WEBPACK_IMPORTED_MODULE_37__PDarbar_pages_add_dish_add_dish__["a" /* addDishPage */],
                __WEBPACK_IMPORTED_MODULE_38__PDarbar_pages_registration_registration__["a" /* RegistrationPage */],
                __WEBPACK_IMPORTED_MODULE_39__PDarbar_pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_40__PDarbar_pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_41__PDarbar_pages_prelogin_prelogin__["a" /* PreloginPage */],
                //directive
                __WEBPACK_IMPORTED_MODULE_30__PDarbar_directives_on_valid_submit_on_valid_submit__["a" /* OnValidSubmitDirective */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/favourites/favourites.module#FavouritesPageModule', name: 'FavouritesPage', segment: 'favourites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/quote/quote.module#QuotePageModule', name: 'QuotePage', segment: 'quote', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/quotes/quotes.module#QuotesPageModule', name: 'QuotesPage', segment: 'quotes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_16__pages_library_library__["a" /* LibraryPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_quotes_quotes__["a" /* QuotesPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_quote_quote__["a" /* QuotePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_favourites_favourites__["a" /* FavouritesPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_24__RecipeApp_pages_tab_tab__["a" /* TabPage */],
                __WEBPACK_IMPORTED_MODULE_25__RecipeApp_pages_receipe_receipe__["a" /* ReceipePage */],
                __WEBPACK_IMPORTED_MODULE_26__RecipeApp_pages_receipes_receipes__["a" /* ReceipesPage */],
                __WEBPACK_IMPORTED_MODULE_27__RecipeApp_pages_edit_receipe_edit_receipe__["a" /* EditReceipePage */],
                __WEBPACK_IMPORTED_MODULE_28__RecipeApp_pages_shopping_list_shopping_list__["a" /* ShoppingListPage */],
                // darbar
                __WEBPACK_IMPORTED_MODULE_31__PDarbar_pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_32__PDarbar_pages_choosetable_choosetable__["a" /* ChoosetablePage */],
                __WEBPACK_IMPORTED_MODULE_33__PDarbar_pages_additems_additems__["a" /* AdditemsPage */],
                __WEBPACK_IMPORTED_MODULE_35__PDarbar_pages_viewcart_viewcart__["a" /* ViewcartPage */],
                __WEBPACK_IMPORTED_MODULE_36__PDarbar_pages_add_category_add_category__["a" /* AddCategoryPage */],
                __WEBPACK_IMPORTED_MODULE_37__PDarbar_pages_add_dish_add_dish__["a" /* addDishPage */],
                __WEBPACK_IMPORTED_MODULE_38__PDarbar_pages_registration_registration__["a" /* RegistrationPage */],
                __WEBPACK_IMPORTED_MODULE_39__PDarbar_pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_40__PDarbar_pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_41__PDarbar_pages_prelogin_prelogin__["a" /* PreloginPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__shared_globalService__["a" /* GlobalService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__["a" /* Base64 */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_fcm__["a" /* FCM */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_local_notifications__["a" /* LocalNotifications */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__services_quotes__["a" /* QuotesService */], __WEBPACK_IMPORTED_MODULE_23__services_settings__["a" /* SettingsService */], __WEBPACK_IMPORTED_MODULE_29__RecipeApp_service_shoppingList_Service__["a" /* ShoppingListService */],
                __WEBPACK_IMPORTED_MODULE_34__PDarbar_Services_cartservice__["a" /* CartService */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ([
    {
        category: 'inspirational',
        quotes: [
            {
                id: '1',
                person: 'Theodore Roosevelt',
                text: 'Believe you can and you\'re halfway there'
            },
            {
                id: '2',
                person: 'Norman Vincent Peale',
                text: 'Change your thoughts and you change your world.'
            },
            {
                id: '3',
                person: 'Robert H. Schuller',
                text: 'What great thing would you attempt if you knew you could not fail?'
            }
        ],
        icon: 'brush'
    },
    {
        category: 'ability',
        quotes: [
            {
                id: '4',
                person: 'John Wooden',
                text: 'Ability may get you to the top, but it takes character to keep you there.'
            },
            {
                id: '5',
                person: 'Robert Frost',
                text: 'Education is the ability to listen to almost anything without losing your temper.'
            }
        ],
        icon: 'bicycle'
    },
    {
        category: 'enthusiasm',
        quotes: [
            {
                id: '6',
                person: 'Benjamin Disraeli',
                text: 'Every product of genius must be the product of enthusiasm.'
            },
            {
                id: '7',
                person: 'Norman Vincent Peale',
                text: 'Enthusiasm releases the drive to carry you over obstacles and adds significance to all you do.'
            }
        ],
        icon: 'battery-charging'
    },
    {
        category: 'motivational',
        quotes: [
            {
                id: '8',
                person: 'Jim Rohn',
                text: 'Either you run the day or the day runs you.'
            },
            {
                id: '9',
                person: 'Donna Brazile',
                text: 'I was motivated to be different in part because I was different.'
            }
        ],
        icon: 'body'
    }
]);
//# sourceMappingURL=qoutes.js.map

/***/ }),

/***/ 498:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(294);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__RecipeApp_pages_tab_tab__ = __webpack_require__(299);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__PDarbar_pages_registration_registration__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__PDarbar_pages_login_login__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, fcm, localNotifications, menuCtrl, global) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.global = global;
        this.tabPage = __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */];
        this.settingsPage = __WEBPACK_IMPORTED_MODULE_7__pages_settings_settings__["a" /* SettingsPage */];
        this.receipeTabpage = __WEBPACK_IMPORTED_MODULE_8__RecipeApp_pages_tab_tab__["a" /* TabPage */];
        this.loginPage = __WEBPACK_IMPORTED_MODULE_11__PDarbar_pages_login_login__["a" /* LoginPage */];
        this.regstrationpage = __WEBPACK_IMPORTED_MODULE_9__PDarbar_pages_registration_registration__["a" /* RegistrationPage */];
        platform.ready().then(function () {
            _this.showFirstScreen();
            statusBar.styleDefault();
            statusBar.hide();
            splashScreen.hide();
            //   fcm.onNotification().subscribe(data => {
            //     if(data.wasTapped){
            //         console.log("Received in background");
            //       console.log(data);
            //     } else {
            //       console.log("Received in foreground");
            //       console.log(data);
            //       localNotifications.schedule({
            //         id: 1,
            //         text: 'Single ILocalNotification',
            //         sound: 'file://sound.mp3',
            //         data: data
            //       });
            //     }
            //   });
        });
    }
    MyApp.prototype.onLoad = function (page) {
        this.navCtrl.setRoot(page);
        this.menuCtrl.close();
    };
    MyApp.prototype.showFirstScreen = function () {
        var userDetails = this.global.getFromLocalStorageObject(this.global.constants.ls_userdetais);
        if (Object.keys(userDetails).length > 0) {
            this.firstScreen = this.loginPage;
        }
        else {
            this.firstScreen = this.regstrationpage;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('nav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */])
    ], MyApp.prototype, "navCtrl", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/app/app.html"*/'<ion-menu [content]="nav">\n    <ion-header>\n        <ion-toolbar>\n            <ion-title>\n                Menu\n            </ion-title>\n        </ion-toolbar>    \n    </ion-header>\n    <ion-content>\n        <ion-list>\n            <button ion-item (click)="onLoad(tabPage)">\n                <ion-icon name="quote" item-left></ion-icon>\n                Quotes\n            </button>\n            <button ion-item (click)="onLoad(settingsPage)">\n                <ion-icon name="settings" item-left></ion-icon>\n                Settings\n            </button>\n            <button ion-item (click)="onLoad(receipeTabpage)">\n                <ion-icon name="pizza" item-left></ion-icon>\n                Receipe\n            </button>\n            <button ion-item (click)="onLoad(pdarbarPage)">\n                <ion-icon name="pizza" item-left></ion-icon>\n                Darbar\n            </button>\n        </ion-list>\n    </ion-content>  \n</ion-menu>\n<ion-nav [root]="firstScreen" #nav></ion-nav>\n<!-- <ion-nav [root]="pdarbarPage" #nav></ion-nav> -->\n<!-- <ion-nav [root]="rootPage"></ion-nav> -->\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__["a" /* FCM */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_10__shared_globalService__["a" /* GlobalService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceipePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ReceipePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReceipePage = /** @class */ (function () {
    function ReceipePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReceipePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReceipePage');
    };
    ReceipePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-receipe',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/receipe/receipe.html"*/'<!--\n  Generated template for the ReceipePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>receipe</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/receipe/receipe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ReceipePage);
    return ReceipePage;
}());

//# sourceMappingURL=receipe.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditReceipePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EditReceipePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditReceipePage = /** @class */ (function () {
    function EditReceipePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EditReceipePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditReceipePage');
    };
    EditReceipePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-receipe',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/edit-receipe/edit-receipe.html"*/'<!--\n  Generated template for the EditReceipePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>editReceipe</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/RecipeApp/pages/edit-receipe/edit-receipe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], EditReceipePage);
    return EditReceipePage;
}());

//# sourceMappingURL=edit-receipe.js.map

/***/ }),

/***/ 533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShoppingListService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model_ingredient_Class__ = __webpack_require__(534);

var ShoppingListService = /** @class */ (function () {
    function ShoppingListService() {
        this.ingredient = [];
    }
    ShoppingListService.prototype.addItem = function (name, amount) {
        this.ingredient.push(new __WEBPACK_IMPORTED_MODULE_0__model_ingredient_Class__["a" /* Ingredient */](name, amount));
    };
    ShoppingListService.prototype.addItems = function (items) {
        (_a = this.ingredient).push.apply(_a, items);
        var _a;
    };
    ShoppingListService.prototype.removeItem = function (index) {
        this.ingredient.splice(index, 1);
    };
    ShoppingListService.prototype.getItems = function () {
        return this.ingredient.slice();
    };
    return ShoppingListService;
}());

//# sourceMappingURL=shoppingList.Service.js.map

/***/ }),

/***/ 534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Ingredient; });
var Ingredient = /** @class */ (function () {
    function Ingredient(name, amount) {
        this.name = name;
        this.amount = amount;
    }
    return Ingredient;
}());

//# sourceMappingURL=ingredient.Class.js.map

/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnValidSubmitDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnValidSubmitDirective = /** @class */ (function () {
    function OnValidSubmitDirective(el) {
        this.el = el;
        console.log('Hello OnValidSubmitDirective Directive');
    }
    OnValidSubmitDirective.prototype.onsubmit = function () {
        console.log('Hello OnValidSubmitDirective Directive');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], OnValidSubmitDirective.prototype, "onsubmit", null);
    OnValidSubmitDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[on-valid-submit]' // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], OnValidSubmitDirective);
    return OnValidSubmitDirective;
}());

//# sourceMappingURL=on-valid-submit.js.map

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_globalService__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CartService = /** @class */ (function () {
    // Private Functions and Variables
    // private carts = {};
    function CartService(Global) {
        this.Global = Global;
    }
    CartService.prototype.getBookedTables = function () {
        var bookedTables = this.Global.getFromLocalStorageObject("bookedTables");
        if (Object.getOwnPropertyNames(bookedTables).length == 0)
            return [];
        return bookedTables;
    };
    CartService.prototype.saveBookedTables = function (bookedTables) {
        this.Global.setInLocalStorageObject("bookedTables", bookedTables);
    };
    CartService.prototype.getCartData = function () {
        var cart = this.Global.getFromLocalStorageObject("cart");
        return cart;
    };
    CartService.prototype.saveCartData = function (cart) {
        this.Global.setInLocalStorageObject("cart", cart);
    };
    CartService.prototype.create = function (tableno) {
        var carts = this.getCartData();
        var timeStamp = new Date().getTime().toString();
        var cart = {
            dateCreated: timeStamp,
            tableNo: tableno
        };
        carts[tableno] = cart;
        this.saveCartData(carts);
        // return cart;
    };
    CartService.prototype.getItem = function (cartId, productId) {
        var carts = this.getCartData();
        var cart = carts[cartId];
        if (cart) {
            if (cart.items && cart.items.length > 0)
                for (var i = 0; i < cart.items.length; i++) {
                    if (cart.items[i].id == productId)
                        // return cart.items[i];
                        return {
                            item: cart.items[i],
                            index: i
                        };
                }
        }
        return undefined;
    };
    CartService.prototype.getOrCreateCartId = function (tableno) {
        // let cartId = sessionStorage.getItem('cartId');
        var carts = this.getCartData();
        var cartId = carts[tableno];
        if (cartId)
            return tableno;
        if (tableno) {
            this.create(tableno);
            // sessionStorage.setItem('cartId', result.dateCreated);
            return tableno;
        }
    };
    CartService.prototype.updateItem = function (product, change, tableno) {
        var cartId = this.getOrCreateCartId(tableno);
        var carts = this.getCartData();
        var x = this.getItem(cartId, product.id);
        // let index = x.index;
        // let item=x.item;
        // let item = this.getItem(cartId, product.id);
        if (x) {
            var quantity = (x.item.quantity || 0) + change;
            if (quantity === 0) {
                carts[cartId].items.splice(x.index, 1);
                if (carts[cartId].items.length <= 0) {
                    this.clearCart(cartId);
                }
            }
            else {
                carts[cartId].items[x.index].quantity = quantity;
                carts[cartId].items[x.index].price = quantity * carts[cartId].items[x.index].rate;
                carts[cartId].items[x.index].totaldiscount = quantity * carts[cartId].items[x.index].discount;
                carts[cartId].items[x.index].finalprice = carts[cartId].items[x.index].price - carts[cartId].items[x.index].totaldiscount;
            }
        }
        else {
            if (!carts[cartId]["items"]) {
                carts[cartId]["items"] = [];
                var bookedTables = this.getBookedTables();
                bookedTables.push(cartId);
                this.saveBookedTables(bookedTables);
            }
            carts[cartId]["items"].push({
                id: product.id,
                imgUrl: product.imgUrl,
                name: product.name,
                rate: product.rate,
                discount: product.discount,
                quantity: 1,
                price: 1 * product.rate,
                totaldiscount: 1 * product.discount,
                finalprice: product.rate - product.discount,
            });
        }
        this.saveCartData(carts);
    };
    // Public Functions and variables
    CartService.prototype.clearCart = function (tableno) {
        var cartId = this.getOrCreateCartId(tableno);
        var carts = this.getCartData();
        delete carts[cartId];
        var bookedTables = this.getBookedTables();
        var index = bookedTables.indexOf(tableno);
        bookedTables.splice(index, 1);
        this.saveBookedTables(bookedTables);
        this.saveCartData(carts);
    };
    CartService.prototype.getCart = function (tableno) {
        var cartId = this.getOrCreateCartId(tableno);
        var carts = this.getCartData();
        return carts[cartId];
    };
    CartService.prototype.getCartQuantity = function (tableno) {
        var totalQuantity = 0;
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var carts = this.getCartData();
        if (carts[cartId]) {
            var items = carts[cartId]["items"];
            for (var key in items) {
                if (key) {
                    totalQuantity = totalQuantity + items[key].quantity;
                }
            }
        }
        return totalQuantity;
    };
    CartService.prototype.getTotalFinalPrice = function (tableno) {
        var totalFinalPrice = 0;
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var carts = this.getCartData();
        if (carts[cartId]) {
            var items = carts[cartId]["items"];
            for (var key in items) {
                if (key) {
                    totalFinalPrice = totalFinalPrice + items[key].finalprice;
                }
            }
        }
        return totalFinalPrice;
    };
    CartService.prototype.getTotalPrice = function (tableno) {
        var totalPrice = 0;
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var carts = this.getCartData();
        if (carts[cartId]) {
            var items = carts[cartId]["items"];
            for (var key in items) {
                if (key) {
                    totalPrice = totalPrice + items[key].price;
                }
            }
        }
        return totalPrice;
    };
    CartService.prototype.getTotalDiscount = function (tableno) {
        var totalDiscount = 0;
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var carts = this.getCartData();
        if (carts[cartId]) {
            var items = carts[cartId]["items"];
            for (var key in items) {
                if (key) {
                    totalDiscount = totalDiscount + items[key].totaldiscount;
                }
            }
        }
        return totalDiscount;
    };
    CartService.prototype.getitemscount = function (tableno) {
        var totalitems = 0;
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var carts = this.getCartData();
        if (carts[cartId]) {
            var items = carts[cartId]["items"];
            for (var key in items) {
                if (key) {
                    totalitems = totalitems + 1;
                }
            }
        }
        return totalitems;
    };
    CartService.prototype.addToCart = function (product, tableno) {
        this.updateItem(product, 1, tableno);
    };
    CartService.prototype.removeFromcart = function (product, tableno) {
        this.updateItem(product, -1, tableno);
    };
    CartService.prototype.checkifpresent = function (product, tableno) {
        // let cartId = this.getOrCreateCartId(tableno);
        var cartId = tableno;
        var x = this.getItem(cartId, product.id);
        if (x) {
            return true;
        }
        else {
            return false;
        }
        // return x?true:false
        // if (!this.carts[cartId]["items"])
        //     this.carts[cartId]["items"] = []
        // return this.carts[cartId]["items"].hasOwnProperty(product.id);
    };
    CartService.prototype.getItemPrice = function (product, tableno) {
        if (this.checkifpresent(product, tableno)) {
            // let cartId = this.getOrCreateCartId(tableno);
            var cartId = tableno;
            var carts = this.getCartData();
            var x = this.getItem(cartId, product.id);
            if (x)
                return carts[cartId]["items"][x.index].finalprice;
        }
    };
    CartService.prototype.getItemDiscount = function (product, tableno) {
        if (this.checkifpresent(product, tableno)) {
            // let cartId = this.getOrCreateCartId(tableno);
            var cartId = tableno;
            var carts = this.getCartData();
            var x = this.getItem(cartId, product.id);
            if (x)
                return carts[cartId]["items"][x.index].totaldiscount;
        }
    };
    CartService.prototype.getQuantity = function (product, tableno) {
        if (this.checkifpresent(product, tableno)) {
            // let cartId = this.getOrCreateCartId(tableno);
            var cartId = tableno;
            var carts = this.getCartData();
            var x = this.getItem(cartId, product.id);
            if (x)
                return carts[cartId]["items"][x.index].quantity;
        }
    };
    CartService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_globalService__["a" /* GlobalService */]])
    ], CartService);
    return CartService;
}());

//# sourceMappingURL=cartservice.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_globalService__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
        this.imageUrl = "assets/imgs/click-me.png";
        this.dashboard = __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */];
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        this.userDetails = this.global.getFromLocalStorageObject(this.global.constants.ls_userdetais);
    };
    LoginPage.prototype.onSubmit = function (formData) {
        var _this = this;
        this.global.showLoading();
        var params = {
            "uid": this.userDetails.uid,
            "pin": formData.value.pin,
        };
        this.global.httpPOST({
            url: this.global.constants.urls.login,
            data: JSON.stringify(params),
            success: function (data) {
                _this.global.hideLoading();
                if (data.SUCCESS == "1") {
                    _this.global.setInLocalStorageObject(_this.global.constants.ls_userdetais, {
                        uid: data.userDetails[0].id,
                        name: data.userDetails[0].name,
                        nooftables: data.userDetails[0].nooftable
                    });
                    _this.navCtrl.setRoot(_this.dashboard);
                }
                else {
                    _this.global.showAlert(_this.global.constants.errorAlertHeader, data.message);
                }
            },
            error: function (error) {
                _this.global.hideLoading();
                console.log(error);
            }
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/login/login.html"*/'\n<ion-header>\n    <ion-navbar color="primary">\n      <ion-title>Login</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="appBackground">\n    <img [src]="imageUrl" (click)="selectImage()" class="categoryImage" alt="your image">\n    <form #form="ngForm"  (ngSubmit)="onSubmit(form)">\n        <ion-list>\n            <ion-label floating>welcome,</ion-label>\n            <!-- <ion-item>\n                <ion-label floating>Email Id</ion-label>\n                <ion-input type="email" name="email" ngModel required></ion-input>\n            </ion-item> -->\n            <ion-item>\n                <ion-label floating>Pin</ion-label>\n                <ion-input type="tel" name="pin" ngModel required></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button  color="secondary" type="submit" block [disabled]="!form.valid">Login</button>       \n    </form>\n</ion-content>'/*ion-inline-end:"/home/swxuser/stuff/Important-stuff/training/ionic/ionic2/src/PDarbar/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__shared_globalService__["a" /* GlobalService */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[313]);
//# sourceMappingURL=main.js.map